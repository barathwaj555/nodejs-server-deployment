var express = require("express");
var bodyParser = require('body-parser');
var app = express();
var router = express.Router();
var path = __dirname + '/views/';




app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 


router.get("/",function(req,res, next){
    res.sendFile(path + "gitPage.html");
 
});

router.get("/upload",function(req,res){
    res.sendFile(path + "uploadPage.html");
});

router.post("/loadGit",function(req,res){
    var gitURL = req.body.gitUrl;
    console.log("Got the post request with parameter as " + gitURL);
	var exec = require('child_process').exec;
	exec("cd ServerFiles", function (error, stdout, stderr) 
		{
			console.log(stdout) 
		});
	exec("git status", function (error, stdout, stderr) 
		{
			console.log(stdout) 
		});
    res.sendFile(path + "gitPage.html");
});

app.use("/",router);


app.use("*",function(req,res){
    res.sendFile(path + "404.html");
});

app.listen(3000,function(){
    console.log("Live at Port 3000");
});