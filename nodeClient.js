var http = require('http');
	fs = require('fs');
	
var url = null;

if(process.argv[2] == null)
{
	url = "http://127.0.0.1:2222"
}
else
{
	url = process.argv[2];
}
console.log("Connecting to URL " + url);
http.get(url, function(res) {
    console.log("Got response: " + res.statusCode);
    var content = '';
	var fileNames = [];
	var fileContent = [];
	var i = 0;
	console.log("Status Code " + res.statusCode);
    res.on('data', function(chunk) {
		console.log("Chunk : " + chunk);
		if(chunk.indexOf("FileName") > -1)
		{
			fileNames[i] = chunk.toString().split(":")[1];
			return;
		}
		else if(chunk.indexOf("EOF") > -1)
		{
			return
		}
		fileContent[i] = chunk;
		i++;
    });
    res.on('end', function() {
        //console.log(content);
		for(var k =0 ; k < fileNames.length ; k++)
		{
				fs.writeFile("ClientFiles/" + fileNames[k], fileContent[k], 
				function(err) {
				if(err) {
					return console.log(err);
				}
				console.log("The file was saved!");
			});
		}
	
		});
}).on('error', function(e) {
    console.log("Got error: " + e.message);
});
